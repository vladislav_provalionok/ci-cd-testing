pipeline {
    agent any
    stages {
        stage('checkout') {
            steps {
                bitbucketStatusNotify(
                        buildState: 'INPROGRESS',
                        buildKey: 'checkout',
                        buildName: 'checkout',
                )

                checkout scm

                bitbucketStatusNotify(
                        buildState: 'SUCCESSFUL',
                        buildKey: 'checkout',
                        buildName: 'checkout',
                )
            }
        }

        stage('Clean project') {
            steps {
                bitbucketStatusNotify(
                        buildState: 'INPROGRESS',
                        buildKey: 'clean',
                        buildName: 'Clean',
                )
//                sh 'fastlane clean'
                sh './gradlew clean'

                bitbucketStatusNotify(
                        buildState: 'SUCCESSFUL',
                        buildKey: 'clean',
                        buildName: 'Clean',
                )
            }
        }

        stage('Run tests') {
            steps {
                script {
//                    sh 'fastlane test'
                    sh './gradlew test'
                }
            }
        }
        stage('Build project') {
            steps {
//                sh 'fastlane build'
                sh './gradlew build'
            }
        }
    }
}