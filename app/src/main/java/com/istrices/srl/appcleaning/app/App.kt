package com.istrices.srl.appcleaning.app

import android.app.Application
import com.istrices.srl.appcleaning.BuildConfig
import com.istrices.srl.appcleaning.di.DI
import com.istrices.srl.appcleaning.di.module.AppModule
import com.istrices.srl.appcleaning.di.module.ServerModule
import toothpick.Toothpick
import toothpick.configuration.Configuration

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initDI()
    }

    private fun initDI() {
        val configuration = if (BuildConfig.DEBUG) {
            Configuration.forDevelopment().preventMultipleRootScopes()
        } else {
            Configuration.forProduction()
        }
        Toothpick.setConfiguration(configuration)

        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))
        Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
            .installModules(ServerModule())
    }
}
