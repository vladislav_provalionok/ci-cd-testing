package com.istrices.srl.appcleaning.app.presentation

import com.istrices.srl.appcleaning.core.BasePresenter
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.core.Screens
import com.istrices.srl.appcleaning.di.provider.InternetConnectionObservable
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.data.storage.Prefs
import com.istrices.srl.appcleaning.model.interactor.ActivitiesInteractor
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AppPresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val prefs: Prefs,
    private val internetConnectionObservable: InternetConnectionObservable,
    private val activitiesInteractor: ActivitiesInteractor,
    private val fileLogger: FileLogger
) : BasePresenter<AppView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        internetConnectionObservable
            .startNetworkCallback()
            .subscribe { isNetworkEnable ->
                if (isNetworkEnable) {
                    syncOfflineRequests()
                }
            }
            .connect()
    }

    override fun onDestroy() {
        super.onDestroy()

        errorHandler.onDestroy()
        internetConnectionObservable.stopNetworkCallback()
    }

    fun onAppStarted() {
        if (prefs.accessToken.isEmpty()) {
            router.newRootScreen(Screens.LoginScreen)
        } else {
            router.newRootScreen(Screens.ActivityFlow)
        }
    }

    fun onBackPressed() {
        router.exit()
    }

    private fun syncOfflineRequests() {
        activitiesInteractor
            .syncOfflineRequests()
            .subscribe(
                {
                    fileLogger.addLogs("Sync Requests: success")
                },
                {
                    fileLogger.addLogs("Sync Requests: error (${it.message})")
                }
            )
            .connect()
    }
}
