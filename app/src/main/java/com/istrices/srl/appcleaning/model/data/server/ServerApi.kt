package com.istrices.srl.appcleaning.model.data.server

import com.istrices.srl.appcleaning.model.data.server.response.ActivitiesResponse
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.model.data.server.response.LoginResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface ServerApi {

    @POST("api/login")
    fun login(@Header("Authorization") encodedToken: String): Single<LoginResponse>

    @POST("api/logout")
    fun logout(): Completable

    @GET("api/attivita")
    fun getAllActivities(@Query("attivita") parameters: Array<String>): Single<ActivitiesResponse>

    @GET("api/attivita/{id}")
    fun getActivity(@Path("id") activityId: Int): Single<Activity>

    @PUT("api/attivita/{id}")
    fun startActivity(
        @Path("id") activityId: Int,
        @Query("attivita") parameters: Array<String>
    ): Single<Activity>

    @PUT("api/attivita/{id}")
    fun stopActivity(
        @Path("id") activityId: Int,
        @Query("attivita") parameters: Array<String>
    ): Single<Activity>
}
