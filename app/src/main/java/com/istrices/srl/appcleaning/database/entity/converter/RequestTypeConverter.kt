package com.istrices.srl.appcleaning.database.entity.converter

import androidx.room.TypeConverter
import com.istrices.srl.appcleaning.database.entity.RequestType

class RequestTypeConverter {

    @TypeConverter
    fun getRequestType(typeValue: Int): RequestType {
        return if (0 < typeValue && typeValue < RequestType.values().size) RequestType.values()[typeValue] else RequestType.START_ACTIVITY
    }

    @TypeConverter
    fun getRequestTypeValue(typeValue: RequestType): Int {
        return typeValue.ordinal
    }
}
