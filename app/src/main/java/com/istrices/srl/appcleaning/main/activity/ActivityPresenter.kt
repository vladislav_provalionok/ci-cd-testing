package com.istrices.srl.appcleaning.main.activity

import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.core.BasePresenter
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.core.FlowRouter
import com.istrices.srl.appcleaning.database.entity.ActivityStatus
import com.istrices.srl.appcleaning.di.PrimitiveWrapper
import com.istrices.srl.appcleaning.di.provider.LocationProvider
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.model.interactor.ActivitiesInteractor
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ActivityPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager,
    private val activitiesInteractor: ActivitiesInteractor,
    private val fileLogger: FileLogger,
    private val activityId: PrimitiveWrapper<Int>,
    private val locationProvider: LocationProvider
) : BasePresenter<ActivityView>() {

    private var activity: Activity? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        getActivity()

        syncActivity()
    }

    fun onBackPressed() {
        flowRouter.exit()
    }

    fun onChangeActivityStatusClicked() {
        if (!locationProvider.isApplicationCanGetLocation()) {
            viewState.showErrorMessage(resourceManager.getString(R.string.have_to_turn_on_gps_or_internet))
            return
        }

        val localActivity = activity

        if (localActivity == null) {
            viewState.showErrorMessage(resourceManager.getString(R.string.something_went_wrong))
            return
        }

        when (localActivity.status) {
            ActivityStatus.CAN_BE_STARTED -> {
                activitiesInteractor
                    .startActivity(activityId.value, localActivity.programmingDate)
                    .subscribe(
                        {},
                        {
                            fileLogger.addLogs("Start Activity: error (${it.message})")

                            errorHandler.handleError(it) { message ->
                                viewState.showErrorMessage(
                                    message
                                )
                            }
                        }
                    )
                    .connect()
            }
            ActivityStatus.IN_PROGRESS -> {
                activitiesInteractor
                    .stopActivity(activityId.value, localActivity.programmingDate)
                    .subscribe(
                        {},
                        {
                            fileLogger.addLogs("Stop Activity: error (${it.message})")

                            errorHandler.handleError(it) { message ->
                                viewState.showErrorMessage(
                                    message
                                )
                            }
                        }
                    )
                    .connect()
            }
            else -> {
                // We don't need to handle DONE status because button (btn_activity_status) will be unavailable
            }
        }
    }

    private fun getActivity() {
        activitiesInteractor
            .getActivity(activityId.value)
            .subscribe(
                { remote ->
                    activity = remote
                    viewState.showActivity(remote)
                },
                {
                    fileLogger.addLogs("Get Activity: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }

    private fun syncActivity() {
        activitiesInteractor
            .syncActivity(activityId.value)
            .subscribe(
                {},
                {
                    fileLogger.addLogs("Sync Activity: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }
}
