package com.istrices.srl.appcleaning.main.activityflow

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface ActivityFlowView : MvpView {

    @OneExecution
    fun showErrorMessage(error: String)
}
