package com.istrices.srl.appcleaning.extension

import android.icu.util.Calendar
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

private const val DATE_TEMPLATE_YYYY_MM_DD_WITH_SLASHES = "%d/%02d/%02d"
private const val DATE_TEMPLATE_YYYY_MM_DD_WITH_DASHES = "%d-%02d-%02d"
private const val COLON = ":"

fun Navigator.setLaunchScreen(screen: SupportAppScreen) {

    applyCommands(
        arrayOf(
            BackTo(null),
            Replace(screen)
        )
    )
}

fun Calendar.formatYYYYMMDDWithSlashes(): String {
    return DATE_TEMPLATE_YYYY_MM_DD_WITH_SLASHES.format(
        get(Calendar.YEAR),
        get(Calendar.MONTH) + 1,
        get(Calendar.DAY_OF_MONTH)
    )
}

fun Calendar.formatYYYYMMDDWithDashes(): String {
    return DATE_TEMPLATE_YYYY_MM_DD_WITH_DASHES.format(
        get(Calendar.YEAR),
        get(Calendar.MONTH) + 1,
        get(Calendar.DAY_OF_MONTH)
    )
}

fun String.getHoursAndMinutes(): String {
    return this.substringBeforeLast(COLON)
}

fun timeNow(): String = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
