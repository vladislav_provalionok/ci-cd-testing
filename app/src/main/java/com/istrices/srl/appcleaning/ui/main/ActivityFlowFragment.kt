package com.istrices.srl.appcleaning.ui.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.core.ActivityFlowScreens
import com.istrices.srl.appcleaning.core.FlowFragment
import com.istrices.srl.appcleaning.databinding.FragmentFlowBinding
import com.istrices.srl.appcleaning.extension.setLaunchScreen
import com.istrices.srl.appcleaning.main.activityflow.ActivityFlowPresenter
import com.istrices.srl.appcleaning.main.activityflow.ActivityFlowView
import com.istrices.srl.appcleaning.ui.activity.ActivityFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class ActivityFlowFragment : FlowFragment(), ActivityFlowView {

    private lateinit var binding: FragmentFlowBinding

    @InjectPresenter
    lateinit var presenter: ActivityFlowPresenter

    @ProvidePresenter
    fun providePresenter(): ActivityFlowPresenter =
        scope.getInstance(ActivityFlowPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(ActivityFlowScreens.ActivitiesScreen)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    override fun inflateView(): View {
        return FragmentFlowBinding
            .inflate(this.layoutInflater)
            .also { binding = it }
            .root
    }

    override fun showErrorMessage(error: String) {
        Toast
            .makeText(context, error, Toast.LENGTH_LONG)
            .show()
    }

    private fun setupViews() {
        binding.ivMenu.setOnClickListener {
            binding.lnMenu.visibility =
                if (binding.lnMenu.visibility == View.GONE) View.VISIBLE
                else View.GONE
        }
        binding.tvAgenda.setOnClickListener {
            binding.lnMenu.visibility = View.GONE

            if (childFragmentManager.findFragmentById(R.id.flowContainer_fl) is ActivityFragment) {
                onBackPressed()
            }
        }
        binding.tvLogout.setOnClickListener {
            binding.lnMenu.visibility = View.GONE

            presenter.onLogoutClicked()
        }
    }
}
