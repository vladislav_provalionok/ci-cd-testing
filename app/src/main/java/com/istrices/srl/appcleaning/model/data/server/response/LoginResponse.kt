package com.istrices.srl.appcleaning.model.data.server.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("id_anagrafica")
    val registryId: String,
    @SerializedName("utente")
    val nameAndSurname: String
)
