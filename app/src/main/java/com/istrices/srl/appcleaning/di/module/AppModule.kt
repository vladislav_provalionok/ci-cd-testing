package com.istrices.srl.appcleaning.di.module

import android.content.Context
import com.istrices.srl.appcleaning.app.App
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.database.core.ActivityDatabase
import com.istrices.srl.appcleaning.database.dao.ActivityDao
import com.istrices.srl.appcleaning.di.provider.InternetConnectionObservable
import com.istrices.srl.appcleaning.di.provider.LocationProvider
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import com.istrices.srl.appcleaning.model.system.scheduler.AppSchedulers
import com.istrices.srl.appcleaning.model.system.scheduler.SchedulersProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(
    app: App
) : Module() {

    init {
        bind(Context::class.java)
            .toInstance(app)
        bind(SchedulersProvider::class.java)
            .toInstance(AppSchedulers())
        bind(ResourceManager::class.java)
            .singleton()
        bind(ErrorHandler::class.java)
            .singleton()
        bind(FileLogger::class.java)
            .singleton()
        bind(ActivityDao::class.java)
            .toInstance(ActivityDatabase.getDatabase(app).activityDao())
        bind(LocationProvider::class.java)
            .singleton()
        bind(InternetConnectionObservable::class.java)
            .singleton()

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java)
            .toInstance(cicerone.router)
        bind(NavigatorHolder::class.java)
            .toInstance(cicerone.navigatorHolder)
    }
}
