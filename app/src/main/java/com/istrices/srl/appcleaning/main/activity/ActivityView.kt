package com.istrices.srl.appcleaning.main.activity

import com.istrices.srl.appcleaning.model.data.server.response.Activity
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEnd
import moxy.viewstate.strategy.alias.OneExecution

interface ActivityView : MvpView {

    @OneExecution
    fun showErrorMessage(error: String)

    @AddToEnd
    fun showActivity(activity: Activity)
}
