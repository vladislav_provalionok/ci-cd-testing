package com.istrices.srl.appcleaning.di

import javax.inject.Qualifier

@Qualifier
annotation class ServerPath
