package com.istrices.srl.appcleaning.main.login

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface LoginView : MvpView {

    @OneExecution
    fun showErrorMessage(error: String)
}
