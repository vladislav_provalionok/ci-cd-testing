package com.istrices.srl.appcleaning.app.presentation

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface AppView : MvpView {

    @OneExecution
    fun showErrorMessage(error: String)
}
