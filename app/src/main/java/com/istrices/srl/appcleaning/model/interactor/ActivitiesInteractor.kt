package com.istrices.srl.appcleaning.model.interactor

import android.icu.util.Calendar
import com.istrices.srl.appcleaning.di.provider.LocationProvider
import com.istrices.srl.appcleaning.extension.formatYYYYMMDDWithDashes
import com.istrices.srl.appcleaning.extension.timeNow
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.model.repository.ActivitiesRepository
import com.istrices.srl.appcleaning.model.system.scheduler.SchedulersProvider
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class ActivitiesInteractor @Inject constructor(
    private val activitiesRepository: ActivitiesRepository,
    private val locationProvider: LocationProvider,
    private val schedulers: SchedulersProvider,
) {

    fun getActivities(calendar: Calendar): Observable<List<Activity>> =
        activitiesRepository
            .getActivities(calendar.formatYYYYMMDDWithDashes())

    fun syncActivities(calendar: Calendar): Completable =
        activitiesRepository.syncActivities(calendar.formatYYYYMMDDWithDashes())

    fun getActivity(activityId: Int): Observable<Activity> =
        activitiesRepository
            .getActivity(activityId)

    fun syncActivity(activityId: Int): Completable =
        activitiesRepository.syncActivity(activityId)

    fun startActivity(
        activityId: Int,
        date: String
    ): Completable =
        locationProvider
            .getLocation()
            .observeOn(schedulers.io())
            .flatMapCompletable { location ->
                activitiesRepository
                    .startActivity(
                        activityId,
                        date,
                        timeNow(),
                        location.latitude.toString(),
                        location.longitude.toString()
                    )
                    .observeOn(schedulers.ui())
            }

    fun stopActivity(
        activityId: Int,
        date: String
    ): Completable =
        locationProvider
            .getLocation()
            .observeOn(schedulers.io())
            .flatMapCompletable { location ->
                activitiesRepository
                    .stopActivity(
                        activityId,
                        timeNow(),
                        date,
                        location.latitude.toString(),
                        location.longitude.toString()
                    )
                    .observeOn(schedulers.ui())
            }

    // We didn't add subscribeOn and observeOn here due to the complex logic of sending multiple requests in parallel
    fun syncOfflineRequests(): Completable = activitiesRepository.syncOfflineRequests()
}
