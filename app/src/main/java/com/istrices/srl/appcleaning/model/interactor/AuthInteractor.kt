package com.istrices.srl.appcleaning.model.interactor

import com.istrices.srl.appcleaning.database.dao.ActivityDao
import com.istrices.srl.appcleaning.model.data.server.ServerApi
import com.istrices.srl.appcleaning.model.data.storage.Prefs
import com.istrices.srl.appcleaning.model.system.scheduler.SchedulersProvider
import io.reactivex.Completable
import java.util.Base64
import javax.inject.Inject

class AuthInteractor @Inject constructor(
    private val serverApi: ServerApi,
    private val schedulers: SchedulersProvider,
    private val prefs: Prefs,
    private val activityDao: ActivityDao
) {

    fun login(username: String, password: String): Completable {
        val tokenBeforeEncoding = "$username:$password"

        val tokenAfterEncoding =
            Base64.getEncoder().encodeToString(tokenBeforeEncoding.toByteArray())

        val resultToken = "Basic $tokenAfterEncoding"

        return serverApi
            .login(resultToken)
            .doOnSuccess {
                prefs.accessToken = resultToken
                prefs.currentUserId = it.registryId
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
    }

    fun logout() =
        serverApi
            .logout()
            .doOnComplete {
                prefs.clearPrefs()
                activityDao.removeAllRequests()
                activityDao.removeAllActivities()
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}
