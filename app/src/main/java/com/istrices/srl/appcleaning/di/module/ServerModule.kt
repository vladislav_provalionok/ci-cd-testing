package com.istrices.srl.appcleaning.di.module

import com.google.gson.Gson
import com.istrices.srl.appcleaning.BuildConfig
import com.istrices.srl.appcleaning.di.ServerPath
import com.istrices.srl.appcleaning.di.provider.GsonProvider
import com.istrices.srl.appcleaning.di.provider.OkHttpClientProvider
import com.istrices.srl.appcleaning.di.provider.ServerApiProvider
import com.istrices.srl.appcleaning.model.data.server.ServerApi
import com.istrices.srl.appcleaning.model.data.server.interceptor.HeaderInterceptor
import com.istrices.srl.appcleaning.model.interactor.ActivitiesInteractor
import com.istrices.srl.appcleaning.model.interactor.AuthInteractor
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {

    init {
        // Network
        bind(Gson::class.java)
            .toProvider(GsonProvider::class.java)
            .providesSingleton()
        bind(HeaderInterceptor::class.java)
            .singleton()
        bind(OkHttpClient::class.java)
            .toProvider(OkHttpClientProvider::class.java)
            .providesSingleton()
        bind(String::class.java)
            .withName(ServerPath::class.java)
            .toInstance(BuildConfig.BASE_URL)
        bind(ServerApi::class.java)
            .toProvider(ServerApiProvider::class.java)
            .providesSingleton()
        bind(ActivitiesInteractor::class.java)
            .singleton()
        bind(AuthInteractor::class.java)
            .singleton()
    }
}
