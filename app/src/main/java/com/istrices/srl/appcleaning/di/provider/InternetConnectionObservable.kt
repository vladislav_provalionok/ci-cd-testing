package com.istrices.srl.appcleaning.di.provider

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import io.reactivex.Observable
import javax.inject.Inject

class InternetConnectionObservable @Inject constructor(
    private val context: Context
) {

    var isNetworkConnected = false
        private set

    private var networkCallback: ConnectivityManager.NetworkCallback? = null

    fun startNetworkCallback(): Observable<Boolean> {
        return Observable.create { emitter ->
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            networkCallback = object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    isNetworkConnected = true
                    emitter.onNext(true)
                }

                override fun onLost(network: Network) {
                    isNetworkConnected = false
                    emitter.onNext(false)
                }
            }

            networkCallback?.let {
                connectivityManager.registerDefaultNetworkCallback(it)
            }
        }
    }

    fun stopNetworkCallback() {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        networkCallback?.let {
            connectivityManager.unregisterNetworkCallback(it)
        }
    }
}
