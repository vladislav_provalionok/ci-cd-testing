package com.istrices.srl.appcleaning.di.provider

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import io.reactivex.Single
import javax.inject.Inject

class LocationProvider @Inject constructor(
    context: Context
) {

    private val locationManager: LocationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    @SuppressLint("MissingPermission")
    fun getLocation(): Single<Location> {
        return Single.create { emitter ->
            val locationListener = LocationListener { location -> emitter.onSuccess(location) }
            val provider =
                if (isGPSProviderEnabled()) LocationManager.GPS_PROVIDER else LocationManager.NETWORK_PROVIDER

            locationManager.requestLocationUpdates(
                provider,
                LOCATION_REFRESH_TIME_MILLIS,
                LOCATION_REFRESH_DISTANCE_METERS,
                locationListener
            )

            emitter.setCancellable {
                locationManager.removeUpdates(locationListener)
            }
        }
    }

    fun isApplicationCanGetLocation(): Boolean =
        isGPSProviderEnabled() || isNetworkProviderEnabled()

    private fun isGPSProviderEnabled(): Boolean =
        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

    private fun isNetworkProviderEnabled(): Boolean =
        locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

    companion object {
        private const val LOCATION_REFRESH_TIME_MILLIS: Long = 500
        private const val LOCATION_REFRESH_DISTANCE_METERS: Float = 5f
    }
}
