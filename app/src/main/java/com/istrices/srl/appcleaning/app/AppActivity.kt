package com.istrices.srl.appcleaning.app

import android.os.Bundle
import android.widget.Toast
import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.app.presentation.AppPresenter
import com.istrices.srl.appcleaning.app.presentation.AppView
import com.istrices.srl.appcleaning.core.FlowFragment
import com.istrices.srl.appcleaning.databinding.ActivityAppBinding
import com.istrices.srl.appcleaning.di.DI
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity(), AppView {

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private lateinit var binding: ActivityAppBinding

    private val navigator: Navigator =
        SupportAppNavigator(this, supportFragmentManager, R.id.appContainer_fl)

    private val currentFlowFragment: FlowFragment?
        get() = supportFragmentManager.findFragmentById(R.id.appContainer_fl) as? FlowFragment

    @ProvidePresenter
    fun providePresenter(): AppPresenter =
        Toothpick.openScope(DI.SERVER_SCOPE).getInstance(AppPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        binding = ActivityAppBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        presenter.onAppStarted()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    override fun onBackPressed() {
        currentFlowFragment?.onBackPressed() ?: presenter.onBackPressed()
    }

    override fun showErrorMessage(error: String) {
        Toast
            .makeText(this, error, Toast.LENGTH_LONG)
            .show()
    }
}
