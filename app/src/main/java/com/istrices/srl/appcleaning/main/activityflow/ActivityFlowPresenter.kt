package com.istrices.srl.appcleaning.main.activityflow

import com.istrices.srl.appcleaning.core.BasePresenter
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.core.Screens
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.interactor.AuthInteractor
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ActivityFlowPresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager,
    private val authInteractor: AuthInteractor,
    private val fileLogger: FileLogger
) : BasePresenter<ActivityFlowView>() {

    fun onLogoutClicked() {
        authInteractor
            .logout()
            .subscribe(
                {
                    router.newRootScreen(Screens.LoginScreen)
                },
                {
                    fileLogger.addLogs("Logout: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }
}
