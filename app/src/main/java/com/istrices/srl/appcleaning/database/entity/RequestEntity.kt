package com.istrices.srl.appcleaning.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.istrices.srl.appcleaning.database.entity.converter.RequestTypeConverter

@Entity(tableName = RequestEntityParams.TABLE_NAME)
@TypeConverters(RequestTypeConverter::class)
data class RequestEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = RequestEntityParams.ID_IN_TABLE)
    var id: Int = 0,
    @ColumnInfo(name = RequestEntityParams.REQUEST_TYPE)
    val requestType: RequestType,
    @ColumnInfo(name = RequestEntityParams.ACTIVITY_ID)
    val activityId: Int,
    @ColumnInfo(name = RequestEntityParams.TIME)
    val time: String,
    @ColumnInfo(name = RequestEntityParams.DATE)
    val date: String,
    @ColumnInfo(name = RequestEntityParams.LATITUDE)
    val latitude: String,
    @ColumnInfo(name = RequestEntityParams.LONGITUDE)
    val longitude: String
)

internal class RequestEntityParams {
    companion object {
        const val TABLE_NAME = "request"
        const val ID_IN_TABLE = "id"
        const val REQUEST_TYPE = "request_type"
        const val ACTIVITY_ID = "activity_id"
        const val TIME = "request_time"
        const val DATE = "request_date"
        const val LATITUDE = "request_latitude"
        const val LONGITUDE = "request_longitude"
    }
}

enum class RequestType {
    START_ACTIVITY,
    STOP_ACTIVITY
}
