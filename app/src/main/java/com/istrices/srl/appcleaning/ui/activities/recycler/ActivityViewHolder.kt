package com.istrices.srl.appcleaning.ui.activities.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.database.entity.ActivityStatus
import com.istrices.srl.appcleaning.databinding.ItemActivityBinding
import com.istrices.srl.appcleaning.extension.getHoursAndMinutes
import com.istrices.srl.appcleaning.model.data.server.response.Activity

class ActivityViewHolder(
    private val itemActivityBinding: ItemActivityBinding,
    private val handler: (Int) -> Unit
) : RecyclerView.ViewHolder(itemActivityBinding.root) {

    fun bindItem(activityItem: Activity) {
        itemActivityBinding.tvTime.text = itemView.context.getString(
            R.string.activity_time,
            activityItem.programmingStartTime.getHoursAndMinutes(),
            activityItem.programmingEndTime.getHoursAndMinutes()
        )

        itemActivityBinding.tvPlace.text = activityItem.client

        itemActivityBinding.tvAddress.text = activityItem.address

        itemActivityBinding.tvContact.text = activityItem.referent

        itemActivityBinding.root.setOnClickListener {
            handler.invoke(activityItem.id)
        }

        when (activityItem.status) {
            ActivityStatus.CAN_BE_STARTED -> itemActivityBinding.ivActivityStatus.visibility = View.GONE
            ActivityStatus.IN_PROGRESS -> itemActivityBinding.ivActivityStatus.setImageResource(R.drawable.ic_clock)
            ActivityStatus.DONE -> itemActivityBinding.ivActivityStatus.setImageResource(R.drawable.ic_check_without_borders)
        }
    }
}
