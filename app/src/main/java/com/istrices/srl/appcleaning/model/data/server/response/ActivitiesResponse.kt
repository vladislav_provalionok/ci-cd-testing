package com.istrices.srl.appcleaning.model.data.server.response

import com.google.gson.annotations.SerializedName
import com.istrices.srl.appcleaning.database.entity.ActivityStatus

data class ActivitiesResponse(
    val activities: List<Activity>
)

data class Activity(
    @SerializedName("id")
    val id: Int,
    // TODO change status field
    @SerializedName("status_in_italian")
    val status: ActivityStatus,
    @SerializedName("data_programmazione")
    val programmingDate: String,
    @SerializedName("ora_inizio_programmazione")
    val programmingStartTime: String,
    @SerializedName("ora_fine_programmazione")
    val programmingEndTime: String,
    @SerializedName("cliente")
    val client: String,
    @SerializedName("indirizzo")
    val address: String,
    @SerializedName("referente")
    val referent: String
)
