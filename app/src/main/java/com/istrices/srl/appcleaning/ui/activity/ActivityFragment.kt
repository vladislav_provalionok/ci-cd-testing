package com.istrices.srl.appcleaning.ui.activity

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.core.BaseFragment
import com.istrices.srl.appcleaning.database.entity.ActivityStatus
import com.istrices.srl.appcleaning.databinding.AlertDialogActivityBinding
import com.istrices.srl.appcleaning.databinding.FragmentActivityBinding
import com.istrices.srl.appcleaning.di.PrimitiveWrapper
import com.istrices.srl.appcleaning.extension.getHoursAndMinutes
import com.istrices.srl.appcleaning.main.activity.ActivityPresenter
import com.istrices.srl.appcleaning.main.activity.ActivityView
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

class ActivityFragment : BaseFragment(), ActivityView {

    private lateinit var binding: FragmentActivityBinding

    private var dialog: AlertDialog? = null

    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            if (permissions.entries.find { it.value == false } == null) {
                presenter.onChangeActivityStatusClicked()
            }
        }

    @InjectPresenter
    lateinit var presenter: ActivityPresenter

    @ProvidePresenter
    fun providePresenter(): ActivityPresenter =
        scope.getInstance(ActivityPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        scope.installModules(object : Module() {
            init {
                bind(PrimitiveWrapper::class.java).toInstance(PrimitiveWrapper(tryToGetParcelable()))
            }
        })
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        dialog?.dismiss()
        dialog = null
    }

    override fun inflateView(): View {
        return FragmentActivityBinding
            .inflate(this.layoutInflater)
            .also { binding = it }
            .root
    }

    override fun showErrorMessage(error: String) {
        Toast
            .makeText(context, error, Toast.LENGTH_LONG)
            .show()
    }

    override fun showActivity(activity: Activity) {
        binding.inActivityInfo.tvTime.text = getString(
            R.string.activity_time,
            activity.programmingStartTime.getHoursAndMinutes(),
            activity.programmingEndTime.getHoursAndMinutes()
        )

        binding.inActivityInfo.tvPlace.text = activity.client

        binding.inActivityInfo.tvAddress.text = activity.address

        binding.inActivityInfo.tvContact.text = activity.referent

        // TODO add the remaining time and hide the button if needed
        when (activity.status) {
            ActivityStatus.CAN_BE_STARTED -> {
                binding.btnActivityStatus.setText(R.string.start_activity)
                binding.btnActivityStatus.visibility = View.VISIBLE
            }
            ActivityStatus.IN_PROGRESS -> {
                binding.btnActivityStatus.setText(R.string.end_activity)
                binding.btnActivityStatus.visibility = View.VISIBLE
            }
            ActivityStatus.DONE -> binding.btnActivityStatus.visibility = View.GONE
        }
    }

    private fun setupViews() {
        binding.btnActivityStatus.setOnClickListener {
            openAlertDialog()
        }
    }

    private fun tryToGetParcelable(): Int? {
        return arguments?.getInt(ACTIVITY_ID)
    }

    private fun openAlertDialog() {
        if (dialog != null) {
            dialog?.show()
            return
        }

        AlertDialog.Builder(context).apply {
            val dialogBinding = AlertDialogActivityBinding.inflate(layoutInflater)

            setView(dialogBinding.root)

            dialogBinding.btnConfirm.setOnClickListener {
                dialog?.cancel()

                requestMultiplePermissions.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                )
            }

            dialogBinding.btnReject.setOnClickListener {
                dialog?.cancel()
            }
        }
            .create()
            .also { dialog = it }
            .show()
    }

    companion object {
        const val ACTIVITY_ID = "activity_id"
    }
}
