package com.istrices.srl.appcleaning.model.data.server.interceptor

import com.istrices.srl.appcleaning.model.data.storage.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(
    private val prefs: Prefs
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        builder.addHeader(HEADER_ACCEPT, HEADER_ACCEPT_VALUE)

        val token = prefs.accessToken
        if (token.isNotEmpty()) {
            builder.addHeader(HEADER_AUTHORIZATION, "bearer $token")
        }

        return chain.proceed(builder.build())
    }

    companion object {
        private const val HEADER_ACCEPT = "Accept"
        private const val HEADER_ACCEPT_VALUE = "application/json"
        private const val HEADER_AUTHORIZATION = "Authorization"
    }
}
