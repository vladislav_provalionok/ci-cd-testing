package com.istrices.srl.appcleaning.logger

import com.istrices.srl.appcleaning.BuildConfig
import com.orhanobut.logger.CsvFormatStrategy
import com.orhanobut.logger.DiskLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import java.util.IllegalFormatException
import javax.inject.Inject

class FileLogger @Inject constructor() {

    init {
        createLogger()
    }

    fun addLogs(logs: String) {
        if (BuildConfig.LOGGER_ENABLE) {
            // It can produce crashes because lib not always can convert incoming strings
            try {
                Logger.d(logs, logs)
            } catch (ex: IllegalFormatException) {}
        }
    }

    private fun createLogger() {
        if (BuildConfig.LOGGER_ENABLE) {
            val formatStrategy: FormatStrategy = CsvFormatStrategy.newBuilder()
                .tag("LOGGER_PRE_RELEASE")
                .build()

            Logger.addLogAdapter(DiskLogAdapter(formatStrategy))
        }
    }
}
