package com.istrices.srl.appcleaning.core

import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.model.data.server.exception.ServerException
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val resourceManager: ResourceManager
) {

    fun handleError(exception: Throwable, messageListener: (String) -> Unit = {}) {
        when (exception) {
            is TimeoutException -> {
                messageListener(resourceManager.getString(R.string.timeout_error))
            }
            is UnknownHostException -> {
                messageListener(resourceManager.getString(R.string.network_error))
            }
            is ServerException -> {
                messageListener(exception.message ?: resourceManager.getString(R.string.something_went_wrong))
            }
            else -> {
                messageListener(resourceManager.getString(R.string.unknown_error))
            }
        }
    }

    fun onDestroy() {
    }
}
