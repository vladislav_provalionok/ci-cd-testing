package com.istrices.srl.appcleaning.database.core

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.istrices.srl.appcleaning.database.dao.ActivityDao
import com.istrices.srl.appcleaning.database.entity.ActivityEntity
import com.istrices.srl.appcleaning.database.entity.RequestEntity

@Database(
    entities = [RequestEntity::class, ActivityEntity::class],
    version = 1
)
abstract class ActivityDatabase : RoomDatabase() {

    abstract fun activityDao(): ActivityDao

    companion object {

        private const val DATABASE_NAME = "activity_database"

        private var instance: ActivityDatabase? = null

        fun getDatabase(context: Context): ActivityDatabase {
            synchronized(ActivityDatabase::class) {
                return instance ?: Room
                    .databaseBuilder(
                        context.applicationContext,
                        ActivityDatabase::class.java,
                        DATABASE_NAME
                    )
                    .build()
                    .also {
                        instance = it
                    }
            }
        }
    }
}
