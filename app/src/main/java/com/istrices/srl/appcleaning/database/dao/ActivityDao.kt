package com.istrices.srl.appcleaning.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.istrices.srl.appcleaning.database.entity.ActivityEntity
import com.istrices.srl.appcleaning.database.entity.ActivityEntityParams
import com.istrices.srl.appcleaning.database.entity.ActivityStatus
import com.istrices.srl.appcleaning.database.entity.RequestEntity
import com.istrices.srl.appcleaning.database.entity.RequestEntityParams
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ActivityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(requestEntity: RequestEntity): Completable

    @Query("SELECT * FROM ${RequestEntityParams.TABLE_NAME}")
    fun getAllRequests(): Single<List<RequestEntity>>

    @Query("DELETE FROM ${RequestEntityParams.TABLE_NAME} WHERE ${RequestEntityParams.ID_IN_TABLE} = (:id)")
    fun deleteRequestById(id: Int): Completable

    @Query("DELETE FROM ${RequestEntityParams.TABLE_NAME}")
    fun removeAllRequests()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(activityEntity: ActivityEntity)

    @Query("SELECT * FROM ${ActivityEntityParams.TABLE_NAME} WHERE ${ActivityEntityParams.DATE} = (:date) ORDER BY start_time DESC")
    fun getAllActivities(date: String): Observable<List<ActivityEntity>>

    @Query("SELECT * FROM ${ActivityEntityParams.TABLE_NAME} WHERE ${ActivityEntityParams.ACTIVITY_ID} = (:activityId)")
    fun getActivityByActivityId(activityId: Int): Observable<ActivityEntity>

    @Query("SELECT * FROM ${ActivityEntityParams.TABLE_NAME} WHERE ${ActivityEntityParams.ACTIVITY_ID} = (:activityId)")
    fun getActivityViaActivityId(activityId: Int): ActivityEntity?

    @Query("DELETE FROM ${ActivityEntityParams.TABLE_NAME}")
    fun removeAllActivities()

    @Transaction
    fun updateActivities(activities: List<ActivityEntity>) {
        activities.forEach { activity ->
            updateActivity(activity)
        }
    }

    @Transaction
    fun updateActivity(activity: ActivityEntity) {
        val activityEntityFromDatabase = getActivityViaActivityId(activity.activityId)

        if (activityEntityFromDatabase == null) {
            insert(activity)
        } else {
            val activityEntity = activity.copy(
                id = activityEntityFromDatabase.id,
                activityId = activityEntityFromDatabase.activityId
            )

            insert(activityEntity)
        }
    }

    @Transaction
    fun updateActivityStatus(activityId: Int, status: ActivityStatus) {
        val activityEntityFromDatabase = getActivityViaActivityId(activityId)

        if (activityEntityFromDatabase != null) {
            val updatedActivity = activityEntityFromDatabase.copy(activityStatus = status)

            insert(updatedActivity)
        }
    }
}
