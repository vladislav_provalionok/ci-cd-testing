package com.istrices.srl.appcleaning.main.activities

import android.icu.util.Calendar
import com.istrices.srl.appcleaning.core.ActivityFlowScreens
import com.istrices.srl.appcleaning.core.BasePresenter
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.core.FlowRouter
import com.istrices.srl.appcleaning.extension.formatYYYYMMDDWithSlashes
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.model.interactor.ActivitiesInteractor
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ActivitiesPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager,
    private val activitiesInteractor: ActivitiesInteractor,
    private val fileLogger: FileLogger
) : BasePresenter<ActivitiesView>() {

    private val savedCalendar = Calendar.getInstance()

    private val activities = ArrayList<Activity>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        getAllActivities()

        syncActivities()
    }

    fun onActivityClicked(activityId: Int) {
        flowRouter.navigateTo(ActivityFlowScreens.ActivityScreen(activityId))
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }

    fun onDateChanged(year: Int, month: Int, dayOfMonth: Int) {
        savedCalendar.set(Calendar.MONTH, month)
        savedCalendar.set(Calendar.YEAR, year)
        savedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        syncActivities()

        getAllActivities()
    }

    fun onDatePickerClicked() {
        viewState.showDatePicker(savedCalendar)
    }

    private fun getAllActivities() {
        activitiesInteractor
            .getActivities(savedCalendar)
            .subscribe(
                {
                    activities.clear()
                    activities.addAll(it)

                    viewState.showActivities(activities, savedCalendar.formatYYYYMMDDWithSlashes())
                },
                {
                    fileLogger.addLogs("Get Activities: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }

    private fun syncActivities() {
        activitiesInteractor
            .syncActivities(savedCalendar)
            .subscribe(
                {},
                {
                    fileLogger.addLogs("Sync Activities: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }
}
