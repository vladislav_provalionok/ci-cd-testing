package com.istrices.srl.appcleaning.core

import androidx.core.os.bundleOf
import com.istrices.srl.appcleaning.ui.activities.fragment.ActivitiesFragment
import com.istrices.srl.appcleaning.ui.activity.ActivityFragment
import com.istrices.srl.appcleaning.ui.activity.ActivityFragment.Companion.ACTIVITY_ID
import com.istrices.srl.appcleaning.ui.login.LoginFragment
import com.istrices.srl.appcleaning.ui.main.ActivityFlowFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object ActivityFlow : SupportAppScreen() {

        override fun getFragment() = ActivityFlowFragment()
    }

    object LoginScreen : SupportAppScreen() {

        override fun getFragment() = LoginFragment()
    }
}

object ActivityFlowScreens {

    object ActivitiesScreen : SupportAppScreen() {

        override fun getFragment() = ActivitiesFragment()
    }

    data class ActivityScreen(private val activityId: Int) : SupportAppScreen() {

        override fun getFragment() =
            ActivityFragment().apply { arguments = bundleOf(Pair(ACTIVITY_ID, activityId)) }
    }
}
