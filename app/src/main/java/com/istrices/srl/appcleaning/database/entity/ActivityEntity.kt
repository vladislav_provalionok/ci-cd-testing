package com.istrices.srl.appcleaning.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.istrices.srl.appcleaning.database.entity.converter.ActivityStatusConverter
import com.istrices.srl.appcleaning.model.data.server.response.Activity

@Entity(tableName = ActivityEntityParams.TABLE_NAME)
@TypeConverters(ActivityStatusConverter::class)
data class ActivityEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ActivityEntityParams.ID_IN_TABLE)
    var id: Int = 0,
    @ColumnInfo(name = ActivityEntityParams.ACTIVITY_STATUS)
    val activityStatus: ActivityStatus,
    @ColumnInfo(name = ActivityEntityParams.ACTIVITY_ID)
    val activityId: Int,
    @ColumnInfo(name = ActivityEntityParams.START_TIME)
    val startTime: String,
    @ColumnInfo(name = ActivityEntityParams.END_TIME)
    val endTime: String,
    @ColumnInfo(name = ActivityEntityParams.DATE)
    val date: String,
    @ColumnInfo(name = ActivityEntityParams.CLIENT)
    val client: String,
    @ColumnInfo(name = ActivityEntityParams.ADDRESS)
    val address: String,
    @ColumnInfo(name = ActivityEntityParams.REFERENT)
    val referent: String
)

internal class ActivityEntityParams {
    companion object {
        const val TABLE_NAME = "activity"
        const val ID_IN_TABLE = "id"
        const val ACTIVITY_STATUS = "activity_status"
        const val ACTIVITY_ID = "activity_id"
        const val START_TIME = "start_time"
        const val END_TIME = "end_time"
        const val DATE = "date"
        const val CLIENT = "client"
        const val ADDRESS = "address"
        const val REFERENT = "referent"
    }
}

enum class ActivityStatus {
    CAN_BE_STARTED,
    IN_PROGRESS,
    DONE
}

fun Activity.toActivityEntity(): ActivityEntity {
    return ActivityEntity(
        // TODO understand how to determine status from response
        activityStatus = ActivityStatus.CAN_BE_STARTED,
        activityId = this.id,
        startTime = this.programmingStartTime,
        endTime = this.programmingEndTime,
        date = this.programmingDate,
        client = this.client,
        address = this.address,
        referent = this.referent
    )
}

fun ActivityEntity.toActivity(): Activity {
    return Activity(
        status = this.activityStatus,
        id = this.activityId,
        programmingStartTime = this.startTime,
        programmingEndTime = this.endTime,
        programmingDate = this.date,
        client = this.client,
        address = this.address,
        referent = this.referent
    )
}
