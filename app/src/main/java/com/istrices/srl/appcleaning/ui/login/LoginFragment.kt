package com.istrices.srl.appcleaning.ui.login

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.istrices.srl.appcleaning.core.BaseFragment
import com.istrices.srl.appcleaning.databinding.FragmentLoginBinding
import com.istrices.srl.appcleaning.di.DI
import com.istrices.srl.appcleaning.main.login.LoginPresenter
import com.istrices.srl.appcleaning.main.login.LoginView
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class LoginFragment : BaseFragment(), LoginView {

    override val parentFragmentScopeName = DI.SERVER_SCOPE

    private lateinit var binding: FragmentLoginBinding

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @ProvidePresenter
    fun providePresenter(): LoginPresenter =
        scope.getInstance(LoginPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    override fun inflateView(): View {
        return FragmentLoginBinding
            .inflate(this.layoutInflater)
            .also { binding = it }
            .root
    }

    override fun showErrorMessage(error: String) {
        Toast
            .makeText(context, error, Toast.LENGTH_LONG)
            .show()
    }

    private fun setupViews() {
        binding.btnLogIn.setOnClickListener {
            presenter.login(
                binding.edUsername.text.toString(),
                binding.edPassword.text.toString()
            )
        }
    }
}
