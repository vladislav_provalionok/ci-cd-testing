package com.istrices.srl.appcleaning.database.entity.converter

import androidx.room.TypeConverter
import com.istrices.srl.appcleaning.database.entity.ActivityStatus

class ActivityStatusConverter {

    @TypeConverter
    fun getActivityStatus(typeValue: Int): ActivityStatus {
        return if (0 < typeValue && typeValue < ActivityStatus.values().size) ActivityStatus.values()[typeValue] else ActivityStatus.CAN_BE_STARTED
    }

    @TypeConverter
    fun getActivityStatusValue(typeValue: ActivityStatus): Int {
        return typeValue.ordinal
    }
}
