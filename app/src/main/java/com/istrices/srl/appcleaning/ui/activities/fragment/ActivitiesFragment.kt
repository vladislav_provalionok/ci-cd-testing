package com.istrices.srl.appcleaning.ui.activities.fragment

import android.app.DatePickerDialog
import android.icu.util.Calendar
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.istrices.srl.appcleaning.core.BaseFragment
import com.istrices.srl.appcleaning.databinding.FragmentActivitiesBinding
import com.istrices.srl.appcleaning.main.activities.ActivitiesPresenter
import com.istrices.srl.appcleaning.main.activities.ActivitiesView
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.ui.activities.recycler.ActivitiesAdapter
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class ActivitiesFragment : BaseFragment(), ActivitiesView, DatePickerDialog.OnDateSetListener {

    private val adapter: ActivitiesAdapter by lazy {
        ActivitiesAdapter(presenter::onActivityClicked)
    }

    private var datePickerDialog: DatePickerDialog? = null

    private lateinit var binding: FragmentActivitiesBinding

    @InjectPresenter
    lateinit var presenter: ActivitiesPresenter

    @ProvidePresenter
    fun providePresenter(): ActivitiesPresenter =
        scope.getInstance(ActivitiesPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        datePickerDialog?.dismiss()
        datePickerDialog = null
    }

    override fun inflateView(): View {
        return FragmentActivitiesBinding
            .inflate(this.layoutInflater)
            .also { binding = it }
            .root
    }

    override fun showErrorMessage(error: String) {
        Toast
            .makeText(context, error, Toast.LENGTH_LONG)
            .show()
    }

    override fun showActivities(activities: List<Activity>, date: String) {
        adapter.submitList(activities.toList())

        setDate(date)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        presenter.onDateChanged(year, month, dayOfMonth)
    }

    override fun showDatePicker(calendar: Calendar) {
        if (datePickerDialog != null) {
            datePickerDialog?.show()
            return
        }

        DatePickerDialog(
            requireContext(),
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
            .also { datePickerDialog = it }
            .show()
    }

    private fun setupViews() {
        binding.lnDatePicker.setOnClickListener {
            presenter.onDatePickerClicked()
        }

        binding.rvActivities.layoutManager = LinearLayoutManager(context)
        binding.rvActivities.adapter = adapter
    }

    private fun setDate(date: String) {
        binding.tvSelectedDate.text = date
    }
}
