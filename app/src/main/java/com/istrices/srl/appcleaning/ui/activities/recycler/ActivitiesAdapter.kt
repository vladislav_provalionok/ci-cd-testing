package com.istrices.srl.appcleaning.ui.activities.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.istrices.srl.appcleaning.databinding.ItemActivityBinding
import com.istrices.srl.appcleaning.model.data.server.response.Activity

class ActivitiesAdapter(
    private val handler: (Int) -> Unit
) : ListAdapter<Activity, ActivityViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemActivityBinding = ItemActivityBinding.inflate(layoutInflater, parent, false)

        return ActivityViewHolder(itemActivityBinding, handler)
    }

    override fun onBindViewHolder(holder: ActivityViewHolder, position: Int) {
        holder.bindItem(getItem(position))
    }

    companion object {

        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Activity> =
            object : DiffUtil.ItemCallback<Activity>() {

                override fun areItemsTheSame(oldItem: Activity, newItem: Activity): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(oldItem: Activity, newItem: Activity): Boolean {
                    return oldItem == newItem
                }
            }
    }
}
