package com.istrices.srl.appcleaning.main.login

import com.istrices.srl.appcleaning.R
import com.istrices.srl.appcleaning.core.BasePresenter
import com.istrices.srl.appcleaning.core.ErrorHandler
import com.istrices.srl.appcleaning.core.Screens
import com.istrices.srl.appcleaning.logger.FileLogger
import com.istrices.srl.appcleaning.model.interactor.AuthInteractor
import com.istrices.srl.appcleaning.model.system.resource.ResourceManager
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LoginPresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager,
    private val authInteractor: AuthInteractor,
    private val fileLogger: FileLogger
) : BasePresenter<LoginView>() {

    fun login(username: String, password: String) {
        if (username.isEmpty() || password.isEmpty()) {
            viewState.showErrorMessage(resourceManager.getString(R.string.fields_are_empty))
            return
        }

        authInteractor
            .login(username, password)
            .subscribe(
                {
                    goToActivitiesScreen()
                },
                {
                    fileLogger.addLogs("Login: error (${it.message})")

                    errorHandler.handleError(it) { message -> viewState.showErrorMessage(message) }
                }
            )
            .connect()
    }

    fun onBackPressed() {
        router.exit()
    }

    private fun goToActivitiesScreen() {
        router.newRootScreen(Screens.ActivityFlow)
    }
}
