package com.istrices.srl.appcleaning.main.activities

import android.icu.util.Calendar
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEnd
import moxy.viewstate.strategy.alias.OneExecution

interface ActivitiesView : MvpView {

    @OneExecution
    fun showErrorMessage(error: String)

    @AddToEnd
    fun showActivities(activities: List<Activity>, date: String)

    @OneExecution
    fun showDatePicker(calendar: Calendar)
}
