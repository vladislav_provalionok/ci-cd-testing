package com.istrices.srl.appcleaning.model.data.server.exception

import java.lang.Exception

class ServerException(message: String) : Exception(message)
