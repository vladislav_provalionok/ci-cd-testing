package com.istrices.srl.appcleaning.model.repository

import com.istrices.srl.appcleaning.database.dao.ActivityDao
import com.istrices.srl.appcleaning.database.entity.ActivityStatus
import com.istrices.srl.appcleaning.database.entity.RequestEntity
import com.istrices.srl.appcleaning.database.entity.RequestType
import com.istrices.srl.appcleaning.database.entity.toActivity
import com.istrices.srl.appcleaning.database.entity.toActivityEntity
import com.istrices.srl.appcleaning.di.provider.InternetConnectionObservable
import com.istrices.srl.appcleaning.model.data.server.ServerApi
import com.istrices.srl.appcleaning.model.data.server.response.Activity
import com.istrices.srl.appcleaning.model.data.storage.Prefs
import com.istrices.srl.appcleaning.model.system.scheduler.SchedulersProvider
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class ActivitiesRepository @Inject constructor(
    private val activityDao: ActivityDao,
    private val serverApi: ServerApi,
    private val schedulers: SchedulersProvider,
    private val prefs: Prefs,
    private val internetConnectionObservable: InternetConnectionObservable
) {

    fun getActivities(date: String): Observable<List<Activity>> =
        activityDao
            .getAllActivities(date)
            .subscribeOn(schedulers.io())
            .map { list -> list.map { it.toActivity() } }
            .observeOn(schedulers.ui())

    fun syncActivities(date: String): Completable {
        return Completable.defer {
            if (internetConnectionObservable.isNetworkConnected) {
                serverApi
                    .getAllActivities(arrayOf(date, prefs.currentUserId))
                    .map { list -> list.activities.map { it.toActivityEntity() } }
                    .doOnSuccess { activityDao.updateActivities(it) }
                    .ignoreElement()
                    .subscribeOn(schedulers.io())
            } else {
                Completable.complete()
            }
        }
    }

    fun getActivity(activityId: Int): Observable<Activity> =
        activityDao
            .getActivityByActivityId(activityId)
            .subscribeOn(schedulers.io())
            .map { it.toActivity() }
            .observeOn(schedulers.ui())

    fun syncActivity(activityId: Int): Completable {
        return Completable.defer {
            if (internetConnectionObservable.isNetworkConnected) {
                serverApi
                    .getActivity(activityId)
                    .map { it.toActivityEntity() }
                    .doOnSuccess { activityDao.updateActivity(it) }
                    .ignoreElement()
                    .subscribeOn(schedulers.io())
            } else {
                Completable.complete()
            }
        }
    }

    fun startActivity(
        activityId: Int,
        time: String,
        date: String,
        latitude: String,
        longitude: String
    ): Completable {
        return if (internetConnectionObservable.isNetworkConnected) {
            startActivityOnline(activityId, time, date, latitude, longitude)
        } else {
            startActivityOffline(activityId, time, date, latitude, longitude)
        }
    }

    fun stopActivity(
        activityId: Int,
        time: String,
        date: String,
        latitude: String,
        longitude: String
    ): Completable {
        return if (internetConnectionObservable.isNetworkConnected) {
            stopActivityOnline(activityId, time, latitude, longitude)
        } else {
            stopActivityOffline(activityId, time, date, latitude, longitude)
        }
    }

    fun syncOfflineRequests(): Completable {
        return activityDao
            .getAllRequests()
            .subscribeOn(schedulers.io())
            .flattenAsObservable { it }
            .flatMap { request ->
                val single = if (request.requestType == RequestType.START_ACTIVITY) {
                    startActivityOnline(
                        request.activityId,
                        request.date,
                        request.time,
                        request.latitude,
                        request.longitude
                    )
                        .doOnComplete {
                            activityDao.deleteRequestById(request.id)
                        }
                } else {
                    stopActivityOnline(
                        request.activityId,
                        request.time,
                        request.latitude,
                        request.longitude
                    )
                        .doOnComplete {
                            activityDao.deleteRequestById(request.id)
                        }
                }

                single
                    .subscribeOn(schedulers.io())
                    .toObservable<Int>()
            }
            .ignoreElements()
            .observeOn(schedulers.ui())
    }

    private fun startActivityOnline(
        activityId: Int,
        date: String,
        time: String,
        latitude: String,
        longitude: String
    ): Completable =
        serverApi
            .startActivity(
                activityId,
                arrayOf(time, date, latitude, longitude)
            )
            .doAfterSuccess { activityDao.updateActivity(it.toActivityEntity()) }
            .ignoreElement()

    private fun startActivityOffline(
        activityId: Int,
        date: String,
        time: String,
        latitude: String,
        longitude: String
    ): Completable {
        val request = RequestEntity(
            requestType = RequestType.START_ACTIVITY,
            activityId = activityId,
            time = time,
            date = date,
            latitude = latitude,
            longitude = longitude
        )

        // TODO change to insert request method and return Activity or return Completable everywhere
        return activityDao
            .insert(request)
            .doOnComplete {
                activityDao.updateActivityStatus(activityId, ActivityStatus.IN_PROGRESS)
            }
    }

    private fun stopActivityOffline(
        activityId: Int,
        time: String,
        date: String,
        latitude: String,
        longitude: String
    ): Completable {
        val request = RequestEntity(
            requestType = RequestType.STOP_ACTIVITY,
            activityId = activityId,
            time = time,
            date = date,
            latitude = latitude,
            longitude = longitude
        )

        // TODO change to insert request method and return Activity or return Completable everywhere
        return activityDao
            .insert(request)
            .doOnComplete {
                activityDao.updateActivityStatus(activityId, ActivityStatus.DONE)
            }
    }

    private fun stopActivityOnline(
        activityId: Int,
        time: String,
        latitude: String,
        longitude: String
    ): Completable =
        serverApi
            .stopActivity(
                activityId,
                arrayOf(time, latitude, longitude)
            )
            .doAfterSuccess { activityDao.updateActivity(it.toActivityEntity()) }
            .ignoreElement()
}
